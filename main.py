import os

import pandas as pd


def hello_world():
    print("Hello, world!")


def main():
    hello_world()


def getSum(a: int, b: int) -> int:
    """return a + b

    Args:
        a (int):
        b (int):

    Returns:
        int: a + b
    """
    return a + b


def function_with_many_parameters(
    param1: int,
    param2: int,
    param3: int,
    param4: int,
    param5: int,
    param6: int,
    param7: int,
) -> int:
    """Return sum of parameters

    Args:
        param1 (int): _description_
        param2 (int): _description_
        param3 (int): _description_
        param4 (int): _description_
        param5 (int): _description_
        param6 (int): _description_
        param7 (int): _description_

    Returns:
        int: param1 + param2 + param3 + param4 + param5 + param6 + param7
    """
    return param1 + param2 + param3 + param4 + param5 + param6 + param7


# check settings from pyproject.toml flake8 max-line-length = 88
def checkLongString() -> int:
    """Return sum of parameters

    Returns:
        int: _description_
    """
    param1, param2, param3, param4, param5, param6, param7 = 1, 2, 3, 4, 5, 6, 7

    output = function_with_many_parameters(
        param1, param2, param3, param4, param5, param6, param7
    )
    return output


def read_csv_files(directory: str) -> pd.DataFrame:
    """Read all CSV files in directory

    Args:
        directory (str): _description_

    Raises:
        ValueError: _description_
        ValueError: _description_

    Returns:
        pd.DataFrame: _description_
    """
    # Check directory
    if not os.path.isdir(directory):
        raise ValueError("Указанный путь не является директорией.")

    # Get files from directory
    files = [file for file in os.listdir(directory) if file.endswith(".csv")]

    # Check CSV files
    if len(files) == 0:
        raise ValueError("В указанной директории отсутствуют CSV-файлы.")

    # Read CVS files
    dfs = []
    for file in files:
        file_path = os.path.join(directory, file)
        df = pd.read_csv(file_path)
        dfs.append(df)

    combined_df = pd.concat(dfs, ignore_index=True)
    return combined_df


if __name__ == "__main__":
    main()
